# Software Requirements Defined :
* A requirement is "anything that drives design choices“, (Lawrence 1997).
* The IEEE Standard Glossary of Software Engineering Terminology (1990) defines a requirement as:
    * A condition or capability needed by a user to solve a problem or achieve an objective.
    * A condition or capability that must be met or possessed by a system or system component to satisfy a contract, standard, specification, or other formally imposed document.
    * A documented representation of a condition or capability as in 1 or 2.
* This definition encompasses both the user's view of the requirements (the external behavior of the system) and the developer's view (some under-the-hood characteristics).
* The following definition acknowledges the diversity of requirements types (Sommerville and Sawyer 1997):
    * Requirements are…a specification of what should be implemented. They are descriptions of how the system should behave, or of a system property or attribute. They may be a constraint on the development process of the system

# Levels of Requirements
![](img/levels.jpg)

# Business Requirements
* Represent high-level objectives of the organization or customer who requests the system.
*  Typically come from the funding sponsor for a project, the acquiring customer, the manager of the actual users, the marketing department, or a product visionary.
*  Describe why the organization is implementing the system—the objectives the organization hopes to achieve.
*  It called sometimes a vision and scope document, or a project charter or a market requirements document.
*  Defining the project scope is the first step in controlling the common problem of scope creep.


User Requirements
*  Describe user goals or tasks that the users must be able to perform with the product.
*  Valuable ways to represent user requirements include use cases, scenario descriptions, and event-response tables.
*  User requirements therefore describe what the user will be able to do with the system.
*  An example of a use case is "Make a Reservation" using an airline, a rental car, or a hotel Web site.

# Functional Requirements
*  Specify the software functionality that the developers must build into the product to enable users to accomplish their tasks, thereby satisfying the business requirements.
*  Sometimes called behavioral requirements, these are the traditional "shall" statements:
    * "The system shall e-mail a reservation confirmation to the user.“
*  Functional requirements are documented in a software requirements specification (SRS), which describes as fully as necessary the expected behavior of the software system.
*  The SRS is used in development, testing, quality assurance, project management, and related project functions.

# Non-Functional Requirements
*  In addition to the functional requirements, the SRS contains nonfunctional requirements.
*  These include performance goals and descriptions of quality attributes.
*  Quality attributes augment the description of the product's functionality by describing the product's characteristics in various dimensions that are important either to users or to developers.
*  These characteristics include:
    * usability, 
    * portability, 
    * integrity, 
    * efficiency, and 
    * robustness.
*  Other nonfunctional requirements describe external interfaces between the system and the outside world, and design and implementation constraints.
*  Constraints impose restrictions on the choices available to the developer for design and construction of the product.

# System Requirements
*  The term system requirements describes the top-level requirements for a product that contains multiple subsystems—that is, a system.
*  A system can be all software or it can include both software and hardware subsystems.
*  People are a part of a system, too, so certain system functions might be allocated to human beings.

# Business Rules
*  Business rules include corporate policies, government regulations, industry standards, accounting practices, and computational algorithms.
*  " business rules are not themselves software requirements because they exist outside the boundaries of any specific software system.
*  However, they often restrict who can perform certain use cases or they dictate that the system must contain functionality to comply with the pertinent rules.
*  Sometimes business rules are the origin of specific quality attributes that are implemented in functionality.
*  Therefore, you can trace the genesis of certain functional requirements back to a particular business rule.

# What Requirements Are Not
*  Requirements specifications do not include design or implementation details (other than known constraints), project planning information, or testing information.
*  Separate such items from the requirements so that the requirements activities can focus on understanding what the team intends to build.
*  Projects typically have other kinds of requirements, including development environment requirements, schedule or budget limitations, the need for a tutorial to help new users get up to speed, or requirements for releasing a product and moving it into the support environment.
*  These are project requirements but not product requirements; they don't fall within the scope of this book.

# Case Study: Alfrde.eu
[Please take a look at their website](https://alfred.eu/)

[Access various documents here](https://alfred.eu/downloads/index.html)

[Use Case and Requirement Analysis document](https://alfred.eu/wp-content/uploads/D2.3-User_Stories_And_Requirement_Analysis_v1.4_For_Approval.pdf)